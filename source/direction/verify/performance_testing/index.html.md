---
layout: markdown_page
title: "Category Vision - Performance Testing"
---

- TOC
{:toc}

## Performance Testing

Load testing is a common and typically important stage in the CI process, ensuring changes to the app can scale before they are introduced into production. Be confident in the performance of your changes by ensuring that they are validated against real-world load scenarios.

Advanced deployment strategies like Canary and Blue/Green can mitigate the risk of deploying code that does not scale. However, that is not a valid alternative for a few reasons:
* You can't improve what you don't measure. Running repeatable and consistent performance tests lets you compare results between runs, to ensure performance doesn't slowly degrade over time.
* Canary and Blue/Green do minimize risk, but you are still essentially testing in production. Best practices would dictate that you use CI test what you can, to reduce the frequency and occurrence of rollbacks and errors in production.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=performance%20testing&sort=milestone)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

## What's Next & Why

Up next is [gitlab-ee#9788](https://gitlab.com/gitlab-org/gitlab-ee/issues/9788) which allows users to report on any metric (load testing results, memory usage, etc.) on a Merge Request.  Using Custom Metrics as the primitive, we will build Integrated Load Testing MVC ([gitlab-org#952](https://gitlab.com/groups/gitlab-org/-/epics/952)).  This epic brings performance testing as a category to viable maturity.

## Competitive Landscape

### Azure DevOps

Azure DevOps offers in-product load testing: https://docs.microsoft.com/en-us/azure/devops/test/load-test/get-started-simple-cloud-load-test?view=azure-devops.  This consists of different types of tests including:

* HTTP Archive Based tests
* URL based tests
* Apache JMeter Tests

For URL type tests, the output contains information about the average response time, user load, requests per second, failed requests and errors (if any).

### Travis CI

TBD

### CircleCI

TBD

## Analyst Landscape

We do not engage with analysts for performance testing market analysis.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category.

## Top Customer Issue(s)

There are no top customer issues for this category.

## Top Internal Customer Issue(s)

Integrated Load Testing ([gitlab-ee#3016](https://gitlab.com/gitlab-org/gitlab-ee/issues/3016)) is the most sought after internal customer issue from Quality.  This will help the Quality team dog food more of our solution for their use case as documented here: [team-tasks#96](https://gitlab.com/gitlab-org/quality/team-tasks/issues/96).

## Top Vision Item(s)
The top Vision item is [gitlab-ee#10681](https://gitlab.com/gitlab-org/gitlab-ee/issues/10681) which will add Integrated Load Testing as an Auto DevOps step.  This will mean that what we have defined as viable for this category will now be added to every Auto DevOps pipeline.